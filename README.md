# Zero to Hero
## General
- Harvard: CS50/x
- MIT: 6.00.1
- MIT: 6.00.2
- Safaribooksonline: Clean code - 14h
- Safaribooksonline: Clean Coder - 13h
## Javascript
- Treehouse Javascript tracks - 95h
    - Front end web development track
    - Web design track
    - Full stack javascript track
- Safarionlinebooks: Modern JavaScript From The Beginning  - 22h
- Pluralsight: JavaScript Path - 26h
- Pluralsight: Node Path - 29h
### Javascript: React
- Treehouse: Learn React track - 9h
- Safarionlinebooks: React 16 - The Complete Guide - 32h
- Pluralsight: Building Applications with React and Redux in ES6 - 7h
- Pluralsight: Building Scalable React Apps - 4h
- Pluralsight: Testing React Applications with Jest - 4h
- Safarionlinebooks: MERN Stack Front To Back - 17h
## Java
- Treehouse Java traks - 54h
    - Beginning java
    - Intermediate java
    - Java Web Development
- Safarionlinebooks: Java 8 and 9 fundamentals - 54h
- Pluralsight: Java path - 48h
- Pluralsight: Spring path - 36h
## Server basics
- Safarionlinebooks: Beginning Linux System administration - 3h
- Safarionlinebooks: Linux Under the Hood (req: LFCS) - 10h
- Safarionlinebooks: Modern linux administration (req: LFCS) - 10h
### LFCS 
- Safarionlinebooks: Linux Foundation Certified system administrator (LFCS) - 15h
- Pluralsight: Linux Foundation Certified System Administrator - 38h
## Server infrastructure
### Docker 
- Treehouse: Introduction to Docker
- Docker: Documentation
- Safaribooksonline: Learn Docker - Fundamentals of Docker 18.x
### Kubernetes - CKA & CKAD
- Udacity: Scalable Microservices with Kubernetes - 6h
- EDX: Introduction to Kubernetes - 10h
- Safaribooksonline: Getting Started with Kubernetes - 6h
- CNCF: Kubernetes Fundamentals - 35h
- GitHub: Kubernetes The Hard Way (Text)
### Jenkins
- YouTube: Jenkins Tutorial Videos
- Udemy: Master Jenkins CI For DevOps and Developers 
### AWS
- Safaribooksonline: Amazon Web Services AWS: AWS Fundamentals
#### AWS Certified Cloud Practitioner
- Safaribooksonline: AWS Certified Cloud Practitioner Complete Video Course
#### AWS Certified Developer - Associate
- Safaribooksonline: AWS Certified Developer - Associate
- Udemy: AWS Certified Developer - Associate
#### AWS Certified SysOps Administrator - Associate
- Safaribooksonline: AWS Certified SysOps Administrator Associate
- Udemy: AWS Certified SysOps Administrator - Associate
#### AWS Certified Solutions Architect - Associate
- Safaribooksonline: AWS Certified Solutions Architect (Associate)
- Udemy: AWS Certified Solutions Architect - Associate
### Google Cloud Certs
- Safaribooksonline: GCP: Complete Google Data Engineer and Cloud Architect Guide
## GO
- Treehouse: Go Language Overview
- Pluralsight: The Go Programming Language
- Safaribooksonline: Ultimate Go Programming, Second Edition
- Udemy: Go: The Complete Developer's Guide 
## Python
## Design Patterns
- Safaribooksonline: Design Patterns (Clean Coders Video Series)
## Travis CI
## Software architectures
# NOTES
- Safaribooksonline: LEARNING PATH: DevOps: Master Puppet, Docker Ansible, Kubernetes & Chef the DevOps Way - All in One Guide!
- UX